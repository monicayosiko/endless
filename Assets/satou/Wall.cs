﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour {

    public GameObject wall;
    int x ;
    public GameObject destroywall;
    public List<GameObject> syoukyo;
    int n ;
    int m ;
    int j ;

	// Use this for initialization
	void Start () {

        x = 12;
        n = 0;
        m = 10;
        j = 0;
	}
	
	// Update is called once per frame
	void Update () {

        j++;
        if (j % 10 == 0)
        {
            float y = Random.Range(-4f, 5f);
            destroywall = Instantiate(wall, new Vector2(x, y), Quaternion.identity);
            syoukyo[n] = destroywall;
            x += 2;
            Destroy(syoukyo[m]);
            n++;
            m++;
            if (n == 20)
            {
                n = 0;
            }
            if (m == 20)
            {
                m = 0;
            }
        }
	}
}
