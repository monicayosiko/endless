﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor : MonoBehaviour
{

    public GameObject floor;
    int i = 20;
    int j = 5;
    int n = 15;
    int m = 19;
    public List<GameObject> floorblock;
    public GameObject destroyblock;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        j++;
        if (j % 10 == 0)
        {
            destroyblock = Instantiate(floor, new Vector3(i, -5f, 0f), Quaternion.identity);
            destroyblock.AddComponent<BoxCollider2D>();
            destroyblock.layer = 8;
            i += 2;
            floorblock[n] = destroyblock;
            Destroy(floorblock[m]);
            n++;
            m++;
            if(n == 20){
                n = 0;
            }if(m == 20){
                m = 0;
            }
        }

    }

}
